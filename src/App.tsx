
import { Box, Container } from '@mui/material';
import About from './components/About';
import Footer from './components/Footer';
import Header from './components/Header';
import Projects from './components/Projects';
import Skills from './components/Skills';
import Contact from './components/Contact';

import Certificates from './components/Certificates';


function App() {
  return (
    <Box >
      <Header/>

      <Container>

      <About/>

      <Projects/>

      <Skills />

      <Certificates />

      <Contact />
     
      </Container>
     <Footer/>
     </Box>
  );
}

export default App;

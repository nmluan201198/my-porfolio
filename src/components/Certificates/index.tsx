import { Box, Button, Stack, Typography } from '@mui/material'
import React from 'react'

type Props = {}


function Certificates({}: Props) {
  return (
    <Box id={'certificates'} display={'flex'}  flexDirection={'column'} flex={1} mb={4}>
            <Typography variant='h4' mb={4} fontWeight={'bold'} color={'GrayText'} textAlign={'center'}>Certificates</Typography>

<Stack direction={'row'} spacing={1}>
    <Box flex={1}  height={350}>
        <img src={require('../../assets/images/JavaCert.png')} width={'100%'} height={'100%'}/>
    </Box>

    <Box flex={1}  height={350}>
        <img src={require('../../assets/images/ProjectManagementCert.png')} width={'100%'} height={'100%'}/>
    </Box>
</Stack>
        </Box>
  )
}

export default Certificates
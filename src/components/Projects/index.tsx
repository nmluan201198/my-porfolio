import { Launch } from '@mui/icons-material'
import { Box, IconButton, Link, Stack, Typography } from '@mui/material'
import React from 'react'

type Props = {}

function Projects({ }: Props) {
    return (
        <Box id={'projects'} display={'flex'} flexDirection={'column'} flex={1} minHeight={'80vh'}>
            <Typography variant='h4' mb={4} fontWeight={'bold'} color={'GrayText'} textAlign={'center'}>Projects</Typography>

            <Stack direction={'row'}  alignItems={'center'}>
                <Box display={'flex'} flexDirection={'column'} flex={0.5} justifyContent={'center'} alignItems={'center'}>

                    <Box p={1} width={200} height={200}>
                        <img src='https://play-lh.googleusercontent.com/J478dXr8Dtoo_8LOz72DZCaXkJmy8rVHzBQBLu-h2yv7F8M5UJN1DCgcVKF4U9AjEA=w240-h480-rw' width={'100%'} height={'100%'} />
                    </Box>

                    <Typography my={1} fontWeight={'bold'}>Fado - Săn deal sắm hàng hiệu</Typography>

                    <Typography my={1} >Typescript - React Native</Typography>

                    <IconButton aria-label="delete">
                    <Link href="https://fado.vn"><Launch /></Link>
                        
                    </IconButton>
                </Box>

                <Box  flex={1}>
                    <img src={require('../../assets/images/Slide.png')} width={'100%'} height={'100%'}/>
                </Box>
            </Stack>
        </Box>
    )
}

export default Projects

import { GitHub } from '@mui/icons-material'
import { Box, Button, Stack, Typography } from '@mui/material'
import React from 'react'

type Props = {}

function About({ }: Props) {
    return (
        <Box display={'flex'} flex={1} my={8} minHeight={'60vh'} justifyContent={'center'} >
            <Stack direction={'column'} justifyContent={'center'} alignItems={'center'}>
                <Typography variant='h3' mb={4} fontWeight={'bold'} color={'GrayText'}>Hi, I am <span style={{color:'#698F3F'}}>Nguyen Minh Luan</span></Typography>
                <Typography variant='h4' mb={4} fontWeight={'bold'} color={'GrayText'}>A Front End / Mobile Developer</Typography>
                <Typography mb={4} color={'GrayText'}>I am a graduated student from FPT University.</Typography>

                <Box display={'flex'} alignItems={'center'}>
                    <Button variant='outlined' sx={{borderRadius: 0, marginRight: 2, color:'#698F3F' , borderColor :'#698F3F'}}>resume</Button>

                    <GitHub />
                </Box>
            </Stack>
        </Box>
    )
}

export default About
import { Box, Card, CardContent, Typography } from '@mui/material'
import React from 'react'

type Props = {}

function Contact({ }: Props) {
  return (
    <Box id={'contact'} display={'flex'} flexDirection={'column'} flex={1}>
      <Typography variant='h4' mb={4} fontWeight={'bold'} color={'GrayText'} textAlign={'center'}>Contact</Typography>

      <Card sx={{ maxWidth: 275, alignSelf:'center' }}>
        <CardContent>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            Email: nmluan201198@gmail.com
          </Typography>

          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            Phone: +84-325370123
          </Typography>

        </CardContent>

      </Card>
    </Box>
  )
}

export default Contact
import { Box, Button, Stack, Typography } from '@mui/material'
import React from 'react'

type Props = {}

const skills = ['React', 'React Native', 'HTML', 'CSS', 'JavaScript', 'TypeScript', 'Redux', 'MUI', 'Git', 'Figma']

function Skills({}: Props) {
  return (
    <Box id={'skills'} display={'flex'}  flexDirection={'column'} flex={1} mb={4}>
            <Typography variant='h4' mb={4} fontWeight={'bold'} color={'GrayText'} textAlign={'center'}>Skills</Typography>

<Stack direction={'row'}  spacing={2}  alignItems={'center'} justifyContent={'center'}>
         {skills.map((skill,index) => {
          return <Button  variant="contained" key={index} color='success'>{skill}</Button>
         })}
         </Stack>
        </Box>
  )
}

export default Skills
import React from 'react'
import { AppBar, Box, Button, IconButton, Link, Toolbar, Typography } from '@mui/material'

import MenuIcon from '@mui/icons-material/Menu';

type Props = {}
const navItems = ['Projects', 'Skills', 'Contact', 'Certificates'];

const Header = (props: Props) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
    <AppBar position="static" color='transparent'>
      <Toolbar>
        <Typography variant="h5" fontWeight={'bold'} color={'#698F3F'} component="div" sx={{ flexGrow: 1 }}>
          NML.
        </Typography>

        <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {navItems.map((item) => (
              <Button variant='text' key={item} sx={{ color: 'GrayText' }}>
              <Link color={'#698F3F'} underline='none' href={'#' + item.toLowerCase()}>{item}</Link>
              </Button>
            ))}
          </Box>
      </Toolbar>
    </AppBar>
  </Box>
  )
}

export default Header